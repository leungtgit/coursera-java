/**
 * This is me testing out the Processing library on my own. It is a very simple
 * and very ugly clock that uses arcs.
 * 
 * @author Torrance Leung
 * @version 0.1
 * @since 2018-10-20
 */
package guimodule;

import processing.core.PApplet;


public class MyDisplay extends PApplet {

	public void setup() {
		
		int blue = color(120,120,255);
		size(800, 800, OPENGL);
		background(blue);
		
	}
	
	public void draw() {
		int yellow = color(255,255,0);
		int red = color(255,0,0);
		//draw clock face
		fill(255);
		ellipse(width/2,height/2,width*.8f,height*.8f);
		//draw seconds
		float s = map(second(), 0, 60, 0, TWO_PI) - HALF_PI;
		fill(0);
		arc(width/2, height/2, width*.75f, height*.75f, -HALF_PI, s, PIE);
		//draw minutes
		fill(yellow);
		float m = map(minute() + norm(second(), 0, 60), 0, 60, 0, TWO_PI) - HALF_PI;
		arc(width/2,height/2, width*.70f, height*.70f, -HALF_PI, m, PIE);
		//draw hours
		fill(red);
		float h = map(hour() % 12 + norm(minute(), 0, 60), 0, 12, 0, TWO_PI) - HALF_PI;
		arc(width/2,height/2, width*.65f, height*.65f, -HALF_PI, h, PIE);

	}
}
