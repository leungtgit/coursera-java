package module3;

//for GUI
import processing.core.PApplet;

//for mapping key to value
import java.util.Map;
import java.util.HashMap;
// for reading file data
import java.util.List;


//for geographic mapping
import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.marker.Marker;
import de.fhpotsdam.unfolding.data.PointFeature;
import de.fhpotsdam.unfolding.data.Feature;
import de.fhpotsdam.unfolding.data.GeoJSONReader;
import de.fhpotsdam.unfolding.marker.SimplePointMarker;
import de.fhpotsdam.unfolding.providers.*;
import de.fhpotsdam.unfolding.utils.MapUtils;

import parsing.ParseFeed;

public class LifeExpectancyMap extends PApplet{

	private static final long serialVersionUID = 1L;
	
	// map data by key for geo-political map
	private Map<String, Float> lifeExpByCountry;
	
	// map
	private UnfoldingMap map;
	private UnfoldingMap map2;
	private UnfoldingMap map3;
	private UnfoldingMap currentMap;
	
	private String lifeExpectancyURL = "";
	
	private List<Feature> countries;
	private List<Marker> countryMarkers;
	
	public void setup() {
		
		//size of window
		size(800, 600, OPENGL);
		//set map here
		map = new UnfoldingMap(this, 200, 0, 600, 600, new Microsoft.HybridProvider());
		map2 = new UnfoldingMap(this, 200, 0, 600, 600, new Microsoft.RoadProvider());
		map3 = new UnfoldingMap(this,200, 0, 600, 600, new Microsoft.AerialProvider());
		
		currentMap = map;
		//set zoom
		map.zoomToLevel(2);
		//create event handler
		MapUtils.createDefaultEventDispatcher(this,  map, map2, map3);
		// map life expectancy to countries
		lifeExpByCountry = loadLifeExpectancyFromCSV("../../API_SP.DYN.LE00.IN_DS2_en_csv_v2_10181296.csv");
		// country markers
		countries = GeoJSONReader.loadData(this, "../../data/countries.geo.json");
		countryMarkers = MapUtils.createSimpleMarkers(countries);
		map.addMarkers(countryMarkers);
		map2.addMarkers(countryMarkers);
		map3.addMarkers(countryMarkers);
		shadeCountries();
	}
	
	public void draw() {
		currentMap.draw();
	}
	
	public void keyPressed() {
		if(key == '1') {
			currentMap = map;
		} else if(key == '2') {
			currentMap = map2;
		} else if(key == '3') {
			currentMap = map3;
		}
	}
	
	
	// helper method for loading life expectancy data
	private Map <String, Float> loadLifeExpectancyFromCSV(String fileName) {
		//build map and read file
		Map<String, Float> lifeExpMap = new HashMap<String, Float>();
		String[] rows = loadStrings(fileName);
		// iterate through rows, get country name and life expectancy data
		for(String row : rows) {
			//sprint the string into column array
			String[] columns = row.split(",");
			//get the latest data
			float value = 0f;
			
			
			
			// iterate through all columns
			for(String each : columns) {
				//try to update the value. print any errors. this should keep the latest data
				try { value = Float.parseFloat(each.replaceAll("\"", "")); }
				catch (Exception e) { 
					//System.out.println(e.toString());
					}
				}

			// parse value and handle lack of data in appropriate columns
			// try-catch for handling null strings

			if(value != 0) {
				lifeExpMap.put(columns[1].replaceAll("\"", "").toString(), value);
					
			}
		}

		
		
		
		return lifeExpMap;
	}
	// helper method for getting life expectancy values from hashmap
	private void shadeCountries() {
		//iterate through markers
		for(Marker marker : countryMarkers) {
			//get ID in form of 3 letter country codes
			String countryId = marker.getId();
			
			//filter out any entries that aren't country codes
			if(lifeExpByCountry.containsKey(countryId)) {
				//extract life expectancy and calculate colors
				float lifeExp = lifeExpByCountry.get(countryId);
				//this uses Map to scale life expectancy of years 40-90 to 10-255 for RGB usage
				//Example
				//40 years -> 10
				//90 years -> 255
				int colorLevel = (int) map(lifeExp, 40, 90, 10, 255);
				marker.setColor(color(255-colorLevel, 100, colorLevel));
			}
			else {
				//If no data then "Omae wa mou shindeiru". Color the country gray.
				marker.setColor(color(150, 150, 150));
			}
			
		}
	}
}
