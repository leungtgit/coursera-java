package MyPackage;

import java.util.ArrayList;
import java.util.List;

import de.fhpotsdam.unfolding.UnfoldingMap;
import de.fhpotsdam.unfolding.data.Feature;
import de.fhpotsdam.unfolding.data.GeoJSONReader;
import de.fhpotsdam.unfolding.data.PointFeature;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.marker.AbstractShapeMarker;
import de.fhpotsdam.unfolding.marker.Marker;
import de.fhpotsdam.unfolding.marker.MultiMarker;
import de.fhpotsdam.unfolding.providers.*;
import de.fhpotsdam.unfolding.providers.MBTilesMapProvider;
import de.fhpotsdam.unfolding.utils.MapUtils;
import parsing.ParseFeed;
import processing.core.PApplet;

/** EarthquakeCityMap
 * An application with an interactive map displaying earthquake data.
 * Author: UC San Diego Intermediate Software Development MOOC team
 * @author Torrance Leung
 * Date: July 17, 2015
 * */
public class EarthquakeCityMap extends PApplet {
	
	// We will use member variables, instead of local variables, to store the data
	// that the setup and draw methods will need to access (as well as other methods)
	// You will use many of these variables, but the only one you should need to add
	// code to modify is countryQuakes, where you will store the number of earthquakes
	// per country.
	
	// You can ignore this.  It's to get rid of eclipse warnings
	private static final long serialVersionUID = 1L;

	// IF YOU ARE WORKING OFFILINE, change the value of this variable to true
	private static final boolean offline = false;
	
	/** This is where to find the local tiles, for working without an Internet connection */
	public static String mbTilesString = "blankLight-1-3.mbtiles";
	
	//feed with magnitude 2.5+ Earthquakes
	private String earthquakesURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.atom";
	
	// The files containing city names and info and country names and info
	private String cityFile = "city-data.json";
	private String countryFile = "countries.geo.json";
	
	// The map
	private UnfoldingMap map;
	
	// Markers for each city
	private List<Marker> cityMarkers;
	// Markers for each earthquake
	private List<Marker> quakeMarkers;

	// A List of country markers
	private List<Marker> countryMarkers;
	
	// NEW IN MODULE 5
	private CommonMarker lastSelected;
	private CommonMarker lastClicked;
	
	public void setup() {		
		// (1) Initializing canvas and map tiles
		size(900, 700, OPENGL);
		if (offline) {
		    map = new UnfoldingMap(this, 200, 50, 650, 600, new MBTilesMapProvider(mbTilesString));
		    earthquakesURL = "2.5_week.atom";  // The same feed, but saved August 7, 2015
		}
		else {
			map = new UnfoldingMap(this, 200, 50, 650, 600, new Microsoft.HybridProvider());
			// IF YOU WANT TO TEST WITH A LOCAL FILE, uncomment the next line
		    //earthquakesURL = "2.5_week.atom";
		}
		MapUtils.createDefaultEventDispatcher(this, map);
		
		
		// (2) Reading in earthquake data and geometric properties
	    //     STEP 1: load country features and markers
		List<Feature> countries = GeoJSONReader.loadData(this, countryFile);
		countryMarkers = MapUtils.createSimpleMarkers(countries);
		
		//     STEP 2: read in city data
		List<Feature> cities = GeoJSONReader.loadData(this, cityFile);
		cityMarkers = new ArrayList<Marker>();
		for(Feature city : cities) {
		  cityMarkers.add(new CityMarker(city));
		}
	    
		//     STEP 3: read in earthquake RSS feed
	    List<PointFeature> earthquakes = ParseFeed.parseEarthquake(this, earthquakesURL);
	    quakeMarkers = new ArrayList<Marker>();
	    
	    for(PointFeature feature : earthquakes) {
		  //check if LandQuake
		  if(isLand(feature)) {
		    quakeMarkers.add(new LandQuakeMarker(feature));
		  }
		  // OceanQuakes
		  else {
		    quakeMarkers.add(new OceanQuakeMarker(feature));
		  }
	    }

	    // could be used for debugging
	    printQuakes();
	 		
	    // (3) Add markers to map
	    //     NOTE: Country markers are not added to the map.  They are used
	    //           for their geometric properties
	    map.addMarkers(quakeMarkers);
	    map.addMarkers(cityMarkers);
	    
	}  // End setup
	
	
	public void draw() {
		background(0);
		map.draw();
		drawThreatLines();
		drawSelected();
		addKey();
		
	}
	
	private void drawSelected() {
		if(this.lastSelected != null) {
			//init vars in case I somehow forgot a case that would lead to null values if I don't init them
			float xBoundary = 0;
			float yBoundary = 0;
			float boxWidth = 0;
			String textBoxText = "";
			//if earthquake marker
			if(this.lastSelected instanceof EarthquakeMarker) {
				textBoxText = this.lastSelected.getStringProperty("title");
				boxWidth = this.textWidth(textBoxText) + 8;
				//the box has no way of going beyond the bottom of the screen so we can just lower it by 10 pixels
				yBoundary = this.lastSelected.getScreenPosition(map).y + 10;
			} else //if city marker
				if(this.lastSelected instanceof CityMarker) {
				textBoxText = ((CityMarker) this.lastSelected).getCity() + ", " + ((CityMarker) this.lastSelected).getCountry() +
							  "\n" +  ((CityMarker) this.lastSelected).getPopulation();
				boxWidth = this.textWidth(textBoxText);
				yBoundary = this.lastSelected.getScreenPosition(map).y;
				//This box is longer than the earthquake box so we have to make sure it doesn't go outside of
				//the window
				if(yBoundary + 50 > this.height) {
					yBoundary = yBoundary - 50;
				} else {
					yBoundary = yBoundary + 10;
				}
			}
			xBoundary = this.lastSelected.getScreenPosition(map).x;
			//All textboxes might go outside the window in the X dimension
			//check if it goes past the window boundary. adjust textbox if necessary.
			if(xBoundary + boxWidth > this.width) {
				xBoundary = xBoundary - boxWidth;
				if(xBoundary < 0) {
					xBoundary = 0;
				}
			}
			lastSelected.showTitle(this.g, xBoundary, yBoundary);
			
		}
	}
	
	/** Event handler that gets called automatically when the 
	 * mouse moves.
	 */
	@Override
	public void mouseMoved()
	{
		// clear the last selection
		if (lastSelected != null) {
			lastSelected.setSelected(false);
			lastSelected = null;
		
		}
		selectMarkerIfHover(quakeMarkers);
		selectMarkerIfHover(cityMarkers);
	}
	
	// If there is a marker under the cursor, and lastSelected is null 
	// set the lastSelected to be the first marker found under the cursor
	// Make sure you do not select two markers.
	// 
	private void selectMarkerIfHover(List<Marker> markers)
	{
		//iterate through markers and set as selected. return to prevent from selecting other markers.
		for(Marker m : markers) {
			if(m.isInside(map, mouseX, mouseY) && this.lastSelected == null) {
				m.setSelected(true);
				this.lastSelected = (CommonMarker)m;
				//debug statement. uncomment if you want your console to be spammed mouseover events
				//System.out.println("Mouseover!");
				return;
			}
		}
	}
	
	/** The event handler for mouse clicks
	 * It will display an earthquake and its threat circle of cities
	 * Or if a city is clicked, it will display all the earthquakes 
	 * where the city is in the threat circle
	 */
	@Override
	public void mouseClicked()
	{
		//if lastClicked is not null, deselect last clicked on marker
		if(this.lastClicked != null) {
			//unhide and set to null again
			this.unhideMarkers();
			this.lastClicked = null;
		} else { //else make last selected the last clicked
			this.lastClicked = this.lastSelected;
			
			
			
			//if earthquake. get threat circle and find cities within the radius.
			if(this.lastClicked instanceof EarthquakeMarker) {
				//Get radius and make sure it isn't hidden
				double radius = ((EarthquakeMarker) this.lastClicked).threatCircle();
				this.lastClicked.setHidden(false);
				//for all city markers. hide cities outside the threat circle and unhide cities within
				for(Marker m : this.cityMarkers) {
					if(m.getDistanceTo(this.lastClicked.getLocation()) <= radius) {
						m.setHidden(false);
					} else {
						m.setHidden(true);
					}
				}
				//Hide other quakes
				for(Marker m : this.quakeMarkers) {
					if(m != this.lastClicked) { m.setHidden(true); }
				}
			
				
				
				
			// else if user clicked on a city marker find earthquakes that could threaten it
			} else if(this.lastClicked instanceof CityMarker) {
				for(Marker m : this.quakeMarkers) {
					double radius = ((EarthquakeMarker)m).threatCircle();
					if(m.getDistanceTo(this.lastClicked.getLocation()) <= radius) {
						m.setHidden(false);
					} else {
						m.setHidden(true);
					}
				}
				//Hide other cities
				for(Marker m : this.cityMarkers) {
					if(m != this.lastClicked) { m.setHidden(true); }
				}
			}
		}
	}
	
	private void drawThreatLines() {
		//if selected is EQ, draw lines to cities it affects
		if(this.lastClicked != null) {
			this.g.pushStyle();
			float clickedX = this.lastClicked.getScreenPosition(map).x;
			float clickedY = this.lastClicked.getScreenPosition(map).y;
			stroke(color(255,0,0));
			strokeWeight(4);
			if(this.lastClicked instanceof EarthquakeMarker) {
				for(Marker m : this.cityMarkers) {
					float mX = ((CommonMarker)m).getScreenPosition(map).x;
					float mY = ((CommonMarker)m).getScreenPosition(map).y;
					if(!m.isHidden()) { line(clickedX, clickedY, mX, mY); }
				}
			} else if(this.lastClicked instanceof CityMarker) {
				for(Marker m: this.quakeMarkers) {
					float mX = ((CommonMarker)m).getScreenPosition(map).x;
					float mY = ((CommonMarker)m).getScreenPosition(map).y;
					if(!m.isHidden()) { line(clickedX, clickedY, mX, mY); }
				}
			}
			this.g.popStyle();
		} 
	}
	
	// loop over and unhide all markers
	private void unhideMarkers() {
		for(Marker marker : quakeMarkers) {
			marker.setHidden(false);
		}
			
		for(Marker marker : cityMarkers) {
			marker.setHidden(false);
		}
	}
	
	// helper method to draw key in GUI
	private void addKey() {	
		// Remember you can use Processing's graphics methods here
		fill(255, 250, 240);
		strokeWeight(1);
		stroke(color(0,0,0));
		int xbase = 25;
		int ybase = 50;
		
		rect(xbase, ybase, 150, 250);
		
		fill(0);
		textAlign(LEFT, CENTER);
		textSize(12);
		text("Earthquake Key", xbase+25, ybase+25);
		
		fill(150, 30, 30);
		int tri_xbase = xbase + 35;
		int tri_ybase = ybase + 50;
		triangle(tri_xbase, tri_ybase-CityMarker.TRI_SIZE, tri_xbase-CityMarker.TRI_SIZE, 
				tri_ybase+CityMarker.TRI_SIZE, tri_xbase+CityMarker.TRI_SIZE, 
				tri_ybase+CityMarker.TRI_SIZE);

		fill(0, 0, 0);
		textAlign(LEFT, CENTER);
		text("City Marker", tri_xbase + 15, tri_ybase);
		
		text("Land Quake", xbase+50, ybase+70);
		text("Ocean Quake", xbase+50, ybase+90);
		text("Size ~ Magnitude", xbase+25, ybase+110);
		
		fill(255, 255, 255);
		ellipse(xbase+35, 
				ybase+70, 
				10, 
				10);
		rect(xbase+35-5, ybase+90-5, 10, 10);
		
		fill(color(255, 255, 0));
		ellipse(xbase+35, ybase+140, 12, 12);
		fill(color(0, 0, 255));
		ellipse(xbase+35, ybase+160, 12, 12);
		fill(color(255, 0, 0));
		ellipse(xbase+35, ybase+180, 12, 12);
		
		textAlign(LEFT, CENTER);
		fill(0, 0, 0);
		text("Shallow", xbase+50, ybase+140);
		text("Intermediate", xbase+50, ybase+160);
		text("Deep", xbase+50, ybase+180);

		text("Past hour", xbase+50, ybase+200);
		
		fill(255, 255, 255);
		int centerx = xbase+35;
		int centery = ybase+200;
		ellipse(centerx, centery, 12, 12);

		strokeWeight(2);
		line(centerx-8, centery-8, centerx+8, centery+8);
		line(centerx-8, centery+8, centerx+8, centery-8);
			
	}

	
	
	// Checks whether this quake occurred on land.  If it did, it sets the 
	// "country" property of its PointFeature to the country where it occurred
	// and returns true.  Notice that the helper method isInCountry will
	// set this "country" property already.  Otherwise it returns false.	
	private boolean isLand(PointFeature earthquake) {
		
		// IMPLEMENT THIS: loop over all countries to check if location is in any of them
		// If it is, add 1 to the entry in countryQuakes corresponding to this country.
		for (Marker country : countryMarkers) {
			if (isInCountry(earthquake, country)) {
				return true;
			}
		}
		
		// not inside any country
		return false;
	}
	
	// prints countries with number of earthquakes
	private void printQuakes() {
		int totalWaterQuakes = quakeMarkers.size();
		for (Marker country : countryMarkers) {
			String countryName = country.getStringProperty("name");
			int numQuakes = 0;
			for (Marker marker : quakeMarkers)
			{
				EarthquakeMarker eqMarker = (EarthquakeMarker)marker;
				if (eqMarker.isOnLand()) {
					if (countryName.equals(eqMarker.getStringProperty("country"))) {
						numQuakes++;
					}
				}
			}
			if (numQuakes > 0) {
				totalWaterQuakes -= numQuakes;
				System.out.println(countryName + ": " + numQuakes);
			}
		}
		System.out.println("OCEAN QUAKES: " + totalWaterQuakes);
	}
	
	
	
	// helper method to test whether a given earthquake is in a given country
	// This will also add the country property to the properties of the earthquake feature if 
	// it's in one of the countries.
	// You should not have to modify this code
	private boolean isInCountry(PointFeature earthquake, Marker country) {
		// getting location of feature
		Location checkLoc = earthquake.getLocation();

		// some countries represented it as MultiMarker
		// looping over SimplePolygonMarkers which make them up to use isInsideByLoc
		if(country.getClass() == MultiMarker.class) {
				
			// looping over markers making up MultiMarker
			for(Marker marker : ((MultiMarker)country).getMarkers()) {
					
				// checking if inside
				if(((AbstractShapeMarker)marker).isInsideByLocation(checkLoc)) {
					earthquake.addProperty("country", country.getProperty("name"));
						
					// return if is inside one
					return true;
				}
			}
		}
			
		// check if inside country represented by SimplePolygonMarker
		else if(((AbstractShapeMarker)country).isInsideByLocation(checkLoc)) {
			earthquake.addProperty("country", country.getProperty("name"));
			
			return true;
		}
		return false;
	}

}
